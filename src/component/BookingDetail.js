import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';

export default class BookingDetail extends Component {
    render() {
        return (
            <div style={{ backgroundColor: 'white', margin: 10, padding: 10, borderRadius: 8}}>
                <div className="border-bottom" style={{ borderBottomWidth: 1, borderColor: '#c8cace', padding: 10 }}>
                    <div style={{ flexDirection:'row',  display: 'flex', justifyContent: 'space-between'}}>
                        <div style={{ }}>
                            {moment(this.props.selectedRow.startDate).format('MMM.DD, YYYY')}
                        </div>
                        <div style={{  }}>{moment(this.props.selectedRow.endDate).format('MMM.DD, YYYY')}</div>
                    </div>
                </div>
                <div className="border-bottom" style={{ borderWidth: 1, borderColor: '#c8cace', padding: 10 }}>
                    <div style={{ flexDirection:'row',  display: 'flex', justifyContent: 'space-between', paddingVertical: 10}}>
                        <div style={{ color: 'grey' }}>GUEST</div>
                        <div style={{ }}>
                            {this.props.selectedRow.guest.firstName}
                            {" "}
                            {this.props.selectedRow.guest.lastName}
                        </div>
                    </div>
                    <div style={{ flexDirection:'row',  display: 'flex', justifyContent: 'space-between', paddingVertical: 10}}>
                        <div style={{ color: 'grey' }}>EMAIL</div>
                        <div style={{  }}>
                            {this.props.selectedRow.guest.email}
                        </div>
                    </div>
                    <div style={{ flexDirection:'row',  display: 'flex', justifyContent: 'space-between', paddingVertical: 10}}>
                        <div style={{ color: 'grey' }}>ADDRESS</div>
                        <div style={{ }}>
                            {this.props.selectedRow.guest.address.streetAddress}
                        </div>
                    </div>
                </div>
                <div className="border-bottom" style={{ borderWidth: 1, borderColor: '#c8cace', padding: 10 }}>
                    <div style={{ flexDirection:'row',  display: 'flex', justifyContent: 'space-between', paddingVertical: 10}}>
                        <div style={{ color: 'grey' }}>SOURCE</div>
                        <div style={{ }}>
                            {this.props.selectedRow.source}
                        </div>
                    </div>

                    <div style={{ flexDirection:'row',  display: 'flex', justifyContent: 'space-between', paddingVertical: 10}}>
                        <div style={{ color: 'grey' }}>STATUS</div>
                        <div style={{  }}>
                            {this.props.selectedRow.status}
                        </div>
                    </div>
                    <div style={{ }}>
                        <div style={{ color: 'grey', textAlign: 'left' }}>BOOKING ID</div>
                        <div style={{ textAlign: 'right'}}>
                            {this.props.selectedRow.id.toUpperCase()}
                        </div>
                    </div>
                </div>
                <div style={{padding: 10}}>
                        <div style={{ flexDirection:'row',  display: 'flex', justifyContent: 'space-between'}}>
                        <div style={{ color: 'grey' }}>BOOKING PRICE</div>
                        <div style={{  }}>
                            USD {this.props.price ? this.props.price.toFixed(2) : ''}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
