import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';
import BootstrapTable from 'react-bootstrap-table-next';
import BookingDetail from '../src/component/BookingDetail';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';


const bookingFormatter = (cell, row) => {
    return (
        <span>{cell.substring(cell.length - 4, cell.length).toUpperCase()}</span>
    );
}
const guestFormatter = (cell, row) => {
    return (
        <span>{cell.firstName}{" "}{cell.lastName}</span>
    );
}

const timeFormatter = (cell, row) => {
    return (
        <span>{moment(cell).format("MMM.DD, YYYY")}</span>
    )
}

const statusFormatter = (cell, row) => {
    if (cell==='HOLD') {
        return (<span className="border border-warning" style={{color: 'orange', borderRadius: 8, padding: 5}}>{cell}</span>)
    }
    else if (cell==='RESERVED') {
        return (<span className="border border-success" style={{color: 'green', borderRadius: 8, padding: 5}}>{cell}</span>)
    }
    else {
        return(<span className="border border-danger" style={{color: 'red', borderRadius: 8, padding: 5}}>{cell}</span>)
    }

}

const columns = [{
    dataField: 'id',
    text: 'Booking ID',
    formatter: bookingFormatter,
    sort: true,
}, {
    dataField: 'guest',
    text: 'Guest name',
    formatter: guestFormatter,
    sort: true,
}, {
    dataField: 'startDate',
    text: 'Check-in',
    formatter: timeFormatter,
}, {
    dataField: 'endDate',
    text: 'Check-out',
    formatter: timeFormatter,
},{
    dataField: 'source',
    text: 'Source',
    sort: true,
}, {
    dataField: 'status',
    text: 'Status',
    formatter: statusFormatter,
}];

class App extends Component {
    state = {
        booking: [],
    }
    fetchData = async (query, variables) => {
        try {
            const response = await axios.post('https://arise-simple-graphql-api.herokuapp.com/graphql', {
                query,
                variables
            });
            this.setState({
                booking: response.data.data.bookings,
            })
        } catch (error) {
            this.setState(() => ({ error }))
        }
    }

    componentDidMount() {
      const query = `
          query{
              bookings{
                  id,
                  source,
                  status,
                  guest{
                      firstName,
                      lastName,
                      email,
                      address{
                          streetAddress,
                          city,
                      },
                  },
                  startDate,
                  endDate,
                  bookingPrice{
                      value,
                      currency,
                  },
              }
          }
      `;

      const variables = {};
      this.fetchData(query, variables)
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.selectedRow && !this.state.change) {
            const price = this.state.selectedRow.bookingPrice;
            const url = 'http://free.currencyconverterapi.com/api/v5/convert?q='+price.currency+'_USD&compact=y';

            fetch(url)
                .then(response => response.json())
                .then(res => {
                    const curr = price.currency+'_USD';

                    this.setState({
                        price: price.value*res[curr].val,
                        change: true,
                    })
                })
                .catch(err => console.log(err))
        }
    }
  render() {
      const rowEvents = {
          onClick: (e, row, rowIndex) => {
            this.setState({
                selectedRow: row,
                change: false,
            });
          }
        };

    return (
      <div className="App">
        <div style={{ display:'flex', flexDirection:'row', fontFamily: 'Trebuchet MS' }}>
            <div style={{ width: '70%', padding: 10 }}>
                <div className='font-weight-bold' style={{ padding: 20, textAlign: 'left' }}>Bookings</div>
                { this.state.booking.length !== 0 ? (
                    <BootstrapTable
                        keyField='id'
                        data={this.state.booking}
                        columns={columns}
                        striped
                        hover
                        condensed
                        rowEvents={rowEvents}
                    />)   : null
                }
            </div>
            <div style={{ width: '30%', backgroundColor: '#e3e6ea' }}>
                <div className="border-bottom border-white" style={{ padding: 20, textAlign: 'left' }}>Booking</div>
                { !this.state.selectedRow ? `Please select a booking to see` : (
                    <BookingDetail
                        price={this.state.price}
                        selectedRow={this.state.selectedRow}
                    />
                )}
            </div>
        </div>
    </div>);
  }
}

export default App;
